from flask import Flask, request, Response, jsonify
from psycopg2 import pool, Error
import db_settings as db


app = Flask(__name__)


connection_pool = pool.SimpleConnectionPool(1,1,
                                            user = db.user,
                                            password = db.password,
                                            host = db.host,
                                            port = db.port,
                                            database = db.database)


def create_database():
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                query = '''CREATE TABLE IF NOT EXISTS tasks
                (id SERIAL PRIMARY KEY,
                task text NOT NULL,
                done bool default False
                );'''
                cursor.execute(query)
                reply = "Database created"
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply

create_database()



@app.route('/api/tasks', methods=['GET'])
def get_all_items():
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                query = "SELECT * FROM tasks;"
                cursor.execute(query)
                tasks = [{'id': row[0], 'task': row[1], 'done': bool(row[2])} for row in cursor.fetchall()]
                reply = jsonify({"tasks": tasks})
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply


@app.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_item(task_id):
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                query = "SELECT * from tasks WHERE id=%s;"
                cursor.execute(query, (task_id, ))
                data_from_db = cursor.fetchone()
                task = {'id': data_from_db[0], 'task': data_from_db[1], 'done': bool(data_from_db[2])}
                reply = jsonify(task)
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply


@app.route('/api/tasks', methods=['POST'])
def create_task():
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                request_data = request.get_json()
                task = request_data['task']
                query = "INSERT INTO tasks (task) VALUES (%s);"
                cursor.execute(query, (task, ))
                reply = jsonify(task)
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply


@app.route('/api/tasks/<int:task_id>', methods=['DELETE'])
def del_item(task_id):
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                query = "DELETE FROM tasks WHERE id = %s"
                cursor.execute(query, (task_id, ))
                reply = jsonify({"info": "task deleted"})
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply


@app.route('/api/tasks/<int:task_id>', methods=['PUT'])
def upadate_task(task_id):
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                request_data = request.get_json()
                task = request_data['task']
                status = request_data['done']
                query = "UPDATE tasks SET task = %s, done = %s WHERE id = %s;"
                cursor.execute(query, (task, status, task_id, ))
                reply = jsonify(task)
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply

###
@app.route('/api/tasks/notdone', methods=['GET'])
def get_all_items_not_done():
    try:
        with connection_pool.getconn() as connection:
            with connection.cursor() as cursor:
                query = "SELECT * FROM tasks WHERE done=False;"
                cursor.execute(query)
                tasks = [{'id': row[0], 'task': row[1], 'done': bool(row[2])} for row in cursor.fetchall()]
                reply = jsonify({"tasks": tasks})
    except Error as error:
        reply = jsonify({"error": error})
    finally:
        connection_pool.putconn(connection)
        return reply



if __name__ == "__main__":
	app.run(debug=True)
